#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def repetidos(new):
    # evalua el largo de la lista para que se cumpla que es mayor a cero
    # y muestra la cantidad de elementos repetidos y que elementos son
    if len(new) > 0 :
        print("\nhay {} elementos repetidosen la lista".format(len(new)))
        print([new])
    else:
        print("no hay repeticiones")
    return repetidos


def primera(lista1):
    # los while se encargan de eliminar los strings que estan repetidos
    # muestra elementos unicos presentes en la primera lista1
    while 'que' in lista1:
        lista1.remove('que')
        while 'adios' in lista1:
            lista1.remove('adios')
            print('elementos que estan en la 1ra lista pero no en la 2da:')
            print([lista1])
    return primera


def segunda(lista2):
    # los while se encargan de eliminar los strings que estan repetidos
    # muestra los elementos unicos presentes en la lista2
    while 'que' in lista2:
        lista2.remove('que')
        while 'adios' in lista2:
            lista2.remove('adios')
            print('\nelementos que estan en la 2da lista pero no en la 1ra:')
            print([lista2])
    return segunda


lista1 = set(['hola', 'que', 'tal', 'adios', 'jajaja'])
lista2 = set(['no', 'creo', 'que', 'progra', 'adios'])
print('lista 1:', lista1)
print('lista 2:', lista2)

new = lista1 & lista2
repetidos(new)
print('\n')
primera(lista1)
segunda(lista2)
