#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def eliminar(lista):
    # se eliminaran todas las X de la lista
    while 'X' in lista:
        lista.remove('X')
    # se invertira la lista para ver el mensaje mas claro
    lista.reverse()
    # para ver como se va comportando el programa print(lista)
    return eliminar


def final(lista):
    # la lista tiene separado cada elemento
    # la funcion join los une
    separador =""
    new = [separador.join(lista)]
    print(new)
    return final


string = ['¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt”']
lista = list('¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt”')
# para ver como se va comportando el programa print(lista)
eliminar(lista)
final(lista)
