#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def invertir(lista_ordenada):
    # esta funcion invierte la lista de strings que fue generada aleatoriamente
    lista_ordenada.reverse()
    return invertir


import random

abc = "abcdefghijklmnñopqrstuvwyz"
print('aleatoriamente se usara cualquiera de estas letras:', abc)
# se generan tres letras al azar del abecedario
l1 = random.choice(abc) + random.choice(abc) + random.choice(abc)
l2 = random.choice(abc) + random.choice(abc) + random.choice(abc)
l3 = random.choice(abc) + random.choice(abc) + random.choice(abc)
l4 = random.choice(abc) + random.choice(abc) + random.choice(abc)
l5 = random.choice(abc) + random.choice(abc) + random.choice(abc)
l6 = random.choice(abc) + random.choice(abc) + random.choice(abc)
# se forman los strings
p1 = [l1 + l2]
p2 = [l3 + l4]
p3 = [l5 + l6]
p4 = [l2 + l4]
# se crea la lista que contendra cuatro palabras
lista_ordenada = list(p1) + list(p2) + list(p3) + list(p4)
print('lista generada:', lista_ordenada)
invertir(lista_ordenada)
print('lista invertida:', lista_ordenada)
