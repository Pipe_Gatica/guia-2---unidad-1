#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def menor(lista):
    # ordena la lista de mayor a menor
    lista.sort()
    lista.reverse()
    print('lista de mayor a menor: ', lista)

    return menor


def mayor(lista):
    # ordena la lista de menor a mayor
    lista.sort()
    print('lista de menor a mayor: ', lista)

    return mayor


lista = [5, 7, 24, 90, 13, 2, 56, 12, 89, 38]


print('lista original: ', lista)
k = int(input('ingrese el valor de k: '))
print('\n')

menor(lista)
mayor(lista)

# si hay uno o mas elementos igual a k, se imprimira
if lista.count(k) >= 1:
    print('hay uno o mas elementos igual a k: ', [k])
elif lista.count(k) == 0:
    print('no hay otro elemento que sea igual a k')
