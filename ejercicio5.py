#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def eliminar(n):
    lista_nueva = []
    # se crea una nueva lista con los elementos sin repetirlos
    # en el caso de elememtos repetidos, solo se considerara el primero
    for i in n:
        if i not in lista_nueva:
            lista_nueva.append(i)
    print('lista sin numeros repetidos: ', lista_nueva)
    return eliminar


a = [1, 2, 3, 9]
b = [3, 4, 5, 8]
c = [2, 5, 6, 7]
# n sera la lista que contendra los elememtos de a, b y c
n = list(a) + list(b) + list(c)
print('lista original: ', n)
eliminar(n)
