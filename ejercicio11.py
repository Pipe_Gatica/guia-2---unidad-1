#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def ordenar(lista):
    # los for mediran el largo de cada lista en i y en j
    for i in range(len(lista)):
        for j in range(len(lista)):
            # mientras el largo de i sea menor al de j
            # la lista se ordenara de manera creciente
            if lista[i] < lista[j]:
                temp = lista[j]
                lista[j] = lista[i]
                lista[i] = temp
    print('lista ordenada:', lista)
    return ordenar


print('ingrese 5 valores a la lista')
lista = []
for x in range(1, 6):
    # el usuario debera ingresar 5 valores para llenar la lista
    valores = int(input('valor #{0}: '.format(x)))
    lista.append(valores)

ordenar(lista)
